create table tbl_users(
    id_users varchar(255) not null,
    username varchar(255) not null,
    name varchar(255) not null,
    password varchar(255) not null,
    email varchar(255) not null,

    constraint pk_tbl_users_idusers PRIMARY KEY (id_users)
);

create table tbl_roles(
    id_roles varchar(255) not null,
    name varchar(255) not null,
    description varchar(255) not null,

    constraint pk_tbl_roles_id_roles PRIMARY KEY (id_roles)
);

create table tbl_users_roles(
    id_users varchar(255) not null,
    id_roles varchar(255) not null,

    constraint pk_tbl_users_roles_idusers FOREIGN KEY (id_users) REFERENCES
    tbl_users (id_users),
    constraint pk_tbl_users_roles_idroles FOREIGN KEY (id_roles) REFERENCES
    tbl_roles (id_roles)
);