package com.hendisantika.springbootjwtsample.request;

import com.hendisantika.springbootjwtsample.entity.Category;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/11/18
 * Time: 08.40
 * To change this template use File | Settings | File Templates.
 */
@Data
@Builder
public class CreatedProductRequest {

    @NotBlank
    private String name;

    @NotNull
    @Max(100)
    @Min(1)
    private int stock;

    @NotNull
    private int price;

    @NotNull
    private Category category;

}
