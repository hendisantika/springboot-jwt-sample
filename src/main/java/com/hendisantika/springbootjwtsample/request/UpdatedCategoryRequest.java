package com.hendisantika.springbootjwtsample.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/11/18
 * Time: 08.45
 * To change this template use File | Settings | File Templates.
 */
@Data
@Builder
public class UpdatedCategoryRequest {

    @NotBlank
    private String categoryId;

    @NotBlank
    private String name;

    @NotBlank
    private String description;
}
