package com.hendisantika.springbootjwtsample.repository;

import com.hendisantika.springbootjwtsample.entity.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/11/18
 * Time: 08.37
 * To change this template use File | Settings | File Templates.
 */
public interface RolesRepository extends JpaRepository<Roles, String> {

    Optional<Roles> findByName(String name);
}
