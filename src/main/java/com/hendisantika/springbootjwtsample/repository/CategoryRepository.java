package com.hendisantika.springbootjwtsample.repository;

import com.hendisantika.springbootjwtsample.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/11/18
 * Time: 08.36
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, String> {

    Optional<Category> findByCategoryIdIn(String categoryId);

    Optional<Category> findByNameIn(String name);

    Category findByCategoryId(String categoryId);

    Category findByName(String name);
}