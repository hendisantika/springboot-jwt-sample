package com.hendisantika.springbootjwtsample.service;

import com.hendisantika.springbootjwtsample.entity.Category;
import com.hendisantika.springbootjwtsample.repository.CategoryRepository;
import com.hendisantika.springbootjwtsample.request.CreatedCategoryRequest;
import com.hendisantika.springbootjwtsample.request.GetDetailCategoryIdRequest;
import com.hendisantika.springbootjwtsample.request.UpdatedCategoryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/11/18
 * Time: 08.49
 * To change this template use File | Settings | File Templates.
 */
@Component
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category createdCategory(CreatedCategoryRequest request) {
        Category category = newCategory(request.getName(), request.getDescription());
        categoryRepository.save(category);
        return category;
    }

    @Override
    public Category updatedCategory(UpdatedCategoryRequest request) {

        Category category = null;
        if (request.getCategoryId() != null) {
            category = EditedCategory(
                    request.getCategoryId(),
                    request.getName(),
                    request.getDescription());
        }
        categoryRepository.save(category);
        return category;
    }

    private Category newCategory(String name, String description) {
        return Category.builder()
                .name(name)
                .description(description)
                .build();
    }

    private Category EditedCategory(String categoryId, String name, String description) {
        return Category.builder()
                .categoryId(categoryId)
                .name(name)
                .description(description)
                .build();
    }

    @Override
    public List<Category> listCategory() {
        List<Category> listCategory = new ArrayList<>();
        for (Category category : categoryRepository.findAll()) {
            listCategory.add(category);
        }
        return listCategory;
    }


    @Override
    public Optional<Category> findByCategoryIdIn(String categoryId) {
        return null;
    }

    @Override
    public Category findByCategoryId(GetDetailCategoryIdRequest request) {
        return categoryRepository.findByCategoryId(request.getCategoryId());
    }

    @Override
    public Optional<Category> findByCategoryByNameIn(String name) {
        return null;
    }

    @Override
    public Category findByCategoryByName(String name) {
        return null;
    }
}
