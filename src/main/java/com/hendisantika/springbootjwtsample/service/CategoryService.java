package com.hendisantika.springbootjwtsample.service;

import com.hendisantika.springbootjwtsample.entity.Category;
import com.hendisantika.springbootjwtsample.request.CreatedCategoryRequest;
import com.hendisantika.springbootjwtsample.request.GetDetailCategoryIdRequest;
import com.hendisantika.springbootjwtsample.request.UpdatedCategoryRequest;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/11/18
 * Time: 08.38
 * To change this template use File | Settings | File Templates.
 */
public interface CategoryService {
    Category createdCategory(CreatedCategoryRequest request);

    Category updatedCategory(UpdatedCategoryRequest request);

    List<Category> listCategory();

    Optional<Category> findByCategoryIdIn(String categoryId);

    Category findByCategoryId(GetDetailCategoryIdRequest request);

    Optional<Category> findByCategoryByNameIn(String name);

    Category findByCategoryByName(String name);
}
