package com.hendisantika.springbootjwtsample.service;

import com.hendisantika.springbootjwtsample.entity.Product;
import com.hendisantika.springbootjwtsample.request.CreatedProductRequest;
import com.hendisantika.springbootjwtsample.request.GetDetailProductRequest;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/11/18
 * Time: 08.49
 * To change this template use File | Settings | File Templates.
 */
public interface ProductService {

    Product createdProduct(CreatedProductRequest request);

    Product findByProductId(GetDetailProductRequest request);

    List<Product> listProduct();
}