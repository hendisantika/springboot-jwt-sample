package com.hendisantika.springbootjwtsample.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/11/18
 * Time: 10.40
 * To change this template use File | Settings | File Templates.
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class ReadyUsersException extends RuntimeException {

    public ReadyUsersException(String message) {
        super(message);
    }

    public ReadyUsersException(String message, Throwable cause) {
        super(message, cause);
    }
}