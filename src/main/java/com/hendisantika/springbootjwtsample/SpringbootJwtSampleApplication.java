package com.hendisantika.springbootjwtsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJwtSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJwtSampleApplication.class, args);
    }
}
