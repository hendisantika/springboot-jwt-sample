package com.hendisantika.springbootjwtsample.customize;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/11/18
 * Time: 10.41
 * To change this template use File | Settings | File Templates.
 */
@Component
public class CustomJwtEntryPoint implements AuthenticationEntryPoint {

    private static final Logger logger = LoggerFactory.getLogger(CustomJwtEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException auth)
            throws IOException {

        logger.error("Responding with unauthorized error. Message - {}",
                auth.getMessage());
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                "Maaf proses authorisasi gagal & tidak dapat mengakses resource ini!");

    }
}
