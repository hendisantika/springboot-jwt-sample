package com.hendisantika.springbootjwtsample.customize;

import com.hendisantika.springbootjwtsample.entity.Users;
import com.hendisantika.springbootjwtsample.exception.ResourceNotException;
import com.hendisantika.springbootjwtsample.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/11/18
 * Time: 10.46
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail)
            throws UsernameNotFoundException {

        Users users = usersRepository.findByEmailOrUsername(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() -> new UsernameNotFoundException("user not found with username or email : " + usernameOrEmail));
        return CustomUserPrincipal.create(users);
    }

    @Transactional
    public UserDetails loadUserByIdusers(String idusers) {
        Users users = usersRepository.findById(idusers)
                .orElseThrow(() -> new ResourceNotException("user", "idusers", idusers));
        return CustomUserPrincipal.create(users);
    }
}