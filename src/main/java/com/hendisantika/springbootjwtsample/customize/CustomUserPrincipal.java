package com.hendisantika.springbootjwtsample.customize;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hendisantika.springbootjwtsample.entity.Users;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/11/18
 * Time: 10.43
 * To change this template use File | Settings | File Templates.
 */
@Data
public class CustomUserPrincipal implements UserDetails {

    private String idusers;

    private String name;

    private String username;

    @JsonIgnore
    private String email;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public CustomUserPrincipal(String idUsers,
                               String name,
                               String username,
                               String email,
                               String password,
                               Collection<? extends GrantedAuthority> authorities) {
        this.idusers = idUsers;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }

    public static CustomUserPrincipal create(Users users) {
        List<GrantedAuthority> authorities = users.getRoles()
                .stream().map(roles -> new SimpleGrantedAuthority(roles.getName()))
                .collect(Collectors.toList());

        return new CustomUserPrincipal(users.getIdUsers(),
                users.getName(), users.getUsername(),
                users.getEmail(), users.getPassword(), authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
