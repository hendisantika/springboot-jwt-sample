package com.hendisantika.springbootjwtsample.controller;

import com.hendisantika.springbootjwtsample.entity.Product;
import com.hendisantika.springbootjwtsample.request.CreatedProductRequest;
import com.hendisantika.springbootjwtsample.request.GetDetailProductRequest;
import com.hendisantika.springbootjwtsample.service.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/11/18
 * Time: 10.57
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(value = "/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<List<Product>> listProduct() {
        return Optional.ofNullable(productService.listProduct())
                .map(callbackJSON -> new ResponseEntity<>(callbackJSON, HttpStatus.OK))
                .orElse(new ResponseEntity<List<Product>>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/{productId}")
    public ResponseEntity<Product> findByProductId(@PathVariable String productId) {
        GetDetailProductRequest request = GetDetailProductRequest
                .builder().productId(productId).build();
        Product product = productService.findByProductId(request);
        if (product == null) {
            return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @PostMapping(value = "/created")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Product> createdProduct(@Valid @RequestBody CreatedProductRequest request) {
        return Optional.ofNullable(productService.createdProduct(request))
                .map(callbackJSON -> new ResponseEntity<>(callbackJSON, HttpStatus.CREATED))
                .orElse(new ResponseEntity<Product>(HttpStatus.BAD_REQUEST));
    }
}
