package com.hendisantika.springbootjwtsample.controller;

import com.hendisantika.springbootjwtsample.entity.Category;
import com.hendisantika.springbootjwtsample.request.CreatedCategoryRequest;
import com.hendisantika.springbootjwtsample.request.GetDetailCategoryIdRequest;
import com.hendisantika.springbootjwtsample.request.UpdatedCategoryRequest;
import com.hendisantika.springbootjwtsample.service.CategoryService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/11/18
 * Time: 10.56
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping(value = "/api/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<List<Category>> listCategory() {
        return Optional.ofNullable(categoryService.listCategory())
                .map(callbackJSON -> new ResponseEntity<>(callbackJSON, HttpStatus.OK))
                .orElse(new ResponseEntity<List<Category>>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = "/created", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Category> createdCategory(@Valid @RequestBody CreatedCategoryRequest request) {
        return Optional.ofNullable(categoryService.createdCategory(request))
                .map(callbackJSON -> new ResponseEntity<>(callbackJSON, HttpStatus.CREATED))
                .orElse(new ResponseEntity<Category>(HttpStatus.BAD_REQUEST));
    }

    @PostMapping(value = "/updated")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Category> updatedCategory(@Valid @RequestBody UpdatedCategoryRequest request) {
        return Optional.ofNullable(categoryService.updatedCategory(request))
                .map(callbackJSON -> new ResponseEntity<>(callbackJSON, HttpStatus.OK))
                .orElse(new ResponseEntity<Category>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping(value = "/{categoryId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Category> getDetailedCategoryId(@PathVariable String categoryId) {
        GetDetailCategoryIdRequest request = GetDetailCategoryIdRequest
                .builder().categoryId(categoryId).build();
        Category category = categoryService.findByCategoryId(request);
        if (category == null) {
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Category>(category, HttpStatus.OK);
    }
}
